// Copyright 2022 Andy Balaam
// Released under the MIT license - see COPYING.txt for details.

// How to do some tricky things with selections in browsers.
// Mostly tested in Firefox.

// Notice that many things that look the same on screen are actually different
// in terms of the Selection object that is created. I don't know whether
// there are any differences in behaviour resulting from these.

let editor;
let notes;
let in_progress = false;

function load() {
    editor = document.getElementById("editor")
    notes = document.getElementById("notes")
    document.addEventListener('selectionchange', selectionchange);
}

function selectionchange(_e) {
    if (in_progress) {
        return;
    }
    //console.log(_e); Not getting much value from this
    console.log(document.getSelection());
}

function start(name, note) {
    in_progress = true;
    console.log(name);
    notes.innerHTML = `<b>${name}</b>: ${note}`;
    editor.focus();
}

function end() {
    setTimeout(() => {in_progress = false;}, 0);
    console.log(document.getSelection());
}

function last_text_node() {
    for (let i = editor.childNodes.length - 1; i >= 0; i--) {
        const n = editor.childNodes[i];
        if (n.nodeType === Node.TEXT_NODE && n.textContent !== "\n") {
            return n;
        }
    }
    return null;
}

function cursor_to_start_of_editor() {
    start(
        "cursor_to_start_of_editor",
        `I don't know how to get into this state with mouse/keyboard.
        Behaves like we are at the end of the text.`
    );

    const sel = document.getSelection();
    sel.selectAllChildren(editor);
    sel.collapseToStart();

    end();
}

function cursor_to_end_of_editor() {
    start(
        "cursor_to_end_of_editor",
        `I don't know how to get into this state with mouse/keyboard.
        Behaves like we are at the end of the text.`
    );

    const sel = document.getSelection();
    sel.selectAllChildren(editor);
    sel.collapseToEnd();

    end();
}

function cursor_to_before_first_node() {
    start(
        "cursor_to_before_first_node",
        `In Firefox, this is what you get when click at the beginning of the
        text, or when you navigate to the beginning with cursor keys, or
        when you tab into the div from outside.`
    );

    const sel = document.getSelection();
    sel.selectAllChildren(editor.firstChild);
    sel.collapseToStart();

    end();
}

function cursor_to_after_last_node() {
    start(
        "cursor_to_after_last_node",
        `I don't know how to get into this state with mouse/keyboard.
        Behaves like we are at the end of the text.`
    );

    const sel = document.getSelection();
    sel.selectAllChildren(editor.lastChild);
    sel.collapseToEnd();

    end();
}

function cursor_to_near_end() {
    start(
        "cursor_to_near_end",
        `In Firefox, you can get into this state by clicking at the end, and
        then pressing the down arrow.`
    );

    const offset = editor.childNodes.length - 1;
    const sel = document.getSelection();
    sel.setBaseAndExtent(editor, offset, editor, offset);

    end();
}

function do_cursor_to_end_of_last_text() {
    const lastTextNode = last_text_node();
    const len = lastTextNode.textContent.length;
    const sel = document.getSelection();
    sel.setBaseAndExtent(lastTextNode, len, lastTextNode, len);
}

function cursor_to_end_of_last_text() {
    start(
        "cursor_to_end_of_last_text",
        `In Firefox, this is what you get when you click at the end of
        the text, or when you navigate with the cursor keys to the end.`
    );

    do_cursor_to_end_of_last_text();

    end();
}

function cursor_to_end_of_first_text() {
    start(
        "cursor_to_end_of_first_text",
        `In Firefox, this is what you get when you click at the end of
        the first line, or when you navigate there with the cursor keys.`
    );

    const firstTextNode = editor.firstChild;
    const len = firstTextNode.textContent.length;
    const sel = document.getSelection();
    sel.setBaseAndExtent(firstTextNode, len, firstTextNode, len);

    end();
}

function cursor_to_blank_line() {
    start(
        "cursor_to_blank_line",
        `This is one of the most interesting: there is no text node to choose
        because we want the cursor to be between two &lt;br&gt; tags. In
        Firefox, we are told the selected node is the editor, with an index
        indicating that we are on the second &lt;br&gt; tag.`
    );

    // We ask for the actual editor div as the selected node, and give an
    // index within it that points at the second BR node.
    const sel = document.getSelection();
    sel.setBaseAndExtent(editor, 2, editor, 2);

    end();
}

function cursor_to_second_br() {
    start(
        "cursor_to_second_br",
        `I don't know how to get into this state with mouse/keyboard.
        Behaves like we clicked on the empty line.`
    );

    let secondBrNode = editor.childNodes[2];

    const sel = document.getSelection();
    sel.selectAllChildren(secondBrNode)

    end();
}

function select_all() {
    start(
        "select_all",
        `In Firefox, this is what you get when you click in the text area and
        press Ctrl-a. The actual selection says that the selected node is
        the editor, with indexes from 0 to the number of nodes inside.`
    );

    const sel = document.getSelection();
    sel.selectAllChildren(editor);

    end();
}

function select_start_to_end() {
    start(
        "select_start_to_end",
        `In Firefox, this is what you get when you click and drag from the top
        left of the editor to the bottom right, selecting all text. The actual
        selection is from the beginning of the first text node to the end of
        the last.`
    );

    const lastTextNode = last_text_node();

    const sel = document.getSelection();
    sel.setBaseAndExtent(
        editor.firstChild,
        0,
        lastTextNode,
        lastTextNode.textContent.length
    );

    end();
}

function select_end_to_start() {
    start(
        "select_end_to_start",
        `In Firefox, this is what you get when you click and drag from the
        bottom right of the editor to the top left, selecting all text. The
        actual selection is from the end of the last node to the beginning of
        the first.`
    );

    do_cursor_to_end_of_last_text();
    document.getSelection().extend(editor.firstChild);

    end();
}

function select_part_first_to_part_last() {
    start(
        "select_part_first_to_part_last",
        `In Firefox, this is what you get when you click and drag from part
        way through the first line to part way through the last line.`
    );

    const lastTextNode = last_text_node();

    const sel = document.getSelection();
    sel.setBaseAndExtent(editor.firstChild, 1, lastTextNode, 2);

    end();
}

function select_part_last_to_part_first() {
    start(
        "select_part_last_to_part_first",
        `In Firefox, this is what you get when you click and drag from part
        way through the last line to part way through the first line.`
    );

    const lastTextNode = last_text_node();
    const sel = document.getSelection();
    sel.setBaseAndExtent(lastTextNode, 2, lastTextNode, 2);
    sel.extend(editor.firstChild, 1);

    end();
}

function select_back_from_blank_line() {
    start(
        "select_back_from_blank_line",
        `In Firefox, this is what you get when click and drag from the blank
        line to somewhere in the first line.`
    );

    const sel = document.getSelection();
    sel.setBaseAndExtent(editor, 2, editor, 2);
    sel.extend(editor.firstChild, 2);

    end();
}

function select_to_blank_line() {
    start(
        "select_to_blank_line",
        `In Firefox, this is what you get when click and drag from somewhere
        on the first line down to the blank line, including the line break.`
    );

    const sel = document.getSelection();
    sel.setBaseAndExtent(editor.firstChild, 1, editor, 2);

    end();
}

function select_from_blank_line() {
    start(
        "select_from_blank_line",
        `In Firefox, this is what you get when click and drag from the blank
        line to somewhere in the last line.`
    );

    const lastTextNode = last_text_node();
    const sel = document.getSelection();
    sel.setBaseAndExtent(editor, 2, lastTextNode, 2);

    end();
}

function select_back_to_blank_line() {
    start(
        "select_back_to_blank_line",
        `In Firefox, this is what you get when click and drag from somewhere
        in the last line back to the blank line.`
    );

    const lastTextNode = last_text_node();
    const sel = document.getSelection();
    sel.setBaseAndExtent(lastTextNode, 2, lastTextNode, 2);
    sel.extend(editor, 2);

    end();
}
